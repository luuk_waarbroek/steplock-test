﻿using UnityEngine;
using System.Collections;

public class TriggerAction : MonoBehaviour {

	void OnGUI(){
		if(GUILayout.Button("Move that cube!")){
			IAction action = new TriggerMovement();
			LockStepManager.Instance.AddAction(action);
		}
	}
}
