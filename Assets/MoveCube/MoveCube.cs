﻿using UnityEngine;
using System.Collections;

public class MoveCube : MonoBehaviour, IHasGameFrame {

	bool allowMove = false;
	int curFrame = 0;

	// Here I simply want the cube to move forward each frame
	public void GameFrameTurn (int gameFramesPerSecond)
	{
		// this doesnt work correcly because the while loop is done in 1 frame... how to implement it over the given gameFramesPerSecond?
		if(allowMove){

			// move cube each frame
			while(curFrame < gameFramesPerSecond){

				curFrame++;
				transform.Translate(Vector3.forward * Time.deltaTime);

			}

			allowMove = false;
			curFrame = 0;

		}

	}

	public bool Finished {
		get {
			return true;
		}
	}

	public void SetAllowMove(){
		allowMove = true;
		SceneManager.Manager.GameFrameObjects.Add(this);
	}

}
