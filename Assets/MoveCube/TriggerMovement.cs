﻿using UnityEngine;
using System;

[Serializable]
public class TriggerMovement : IAction {

		
	public void ProcessAction(){
		GameObject.Find("Cube").GetComponent<MoveCube>().SetAllowMove();
	}

}
